package com.example.uapv1600843.tp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import static com.example.uapv1600843.tp2.R.id.nameBook;
import static com.example.uapv1600843.tp2.R.id.editAuthors;
import static com.example.uapv1600843.tp2.R.id.editGenres;
import static com.example.uapv1600843.tp2.R.id.editPublisher;
import static com.example.uapv1600843.tp2.R.id.editYear;
import static com.example.uapv1600843.tp2.R.id.button;


public class bookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("data");
        final EditText name = (EditText) findViewById(nameBook);
        final EditText author = (EditText) findViewById(editAuthors);
        final EditText genre = (EditText) findViewById(editGenres);
        final EditText publi = (EditText) findViewById(editPublisher);
        final EditText year = (EditText) findViewById(editYear);
        final Button save = (Button) findViewById(button);

        name.setText(bundle.getString("title"));
        author.setText(bundle.getString("authors"));
        genre.setText(bundle.getString("genre"));
        publi.setText(bundle.getString("publisher"));
        year.setText(bundle.getString("year"));
        final long id = bundle.getLong("id");

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(bookActivity.this, BibliActivity.class);
                if (id != 0)
                {
                    Book book = new Book(id,name.getText().toString(),author.getText().toString(),year.getText().toString(),genre.getText().toString(),publi.getText().toString());
                    BookDbHelper bd = new BookDbHelper(getApplicationContext());
                    bd.updateBook(book);
                }

                else
                {
                    Book book = new Book(name.getText().toString(),author.getText().toString(),year.getText().toString(),genre.getText().toString(),publi.getText().toString());
                    BookDbHelper bd = new BookDbHelper(getApplicationContext());
                    bd.addBook(book);
                }
                startActivity(intent);
            }

        });
    }


}
