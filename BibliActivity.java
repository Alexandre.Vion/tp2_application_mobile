package com.example.uapv1600843.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.List;

import static com.example.uapv1600843.tp2.R.id.add;
import static com.example.uapv1600843.tp2.R.id.listbibli;

public class BibliActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bibli);
        final ListView listview = (ListView) findViewById(listbibli);
        final FloatingActionButton button = (FloatingActionButton) findViewById(add);
        BookDbHelper bd = new BookDbHelper(getApplicationContext());
        final Cursor cursor = bd.fetchAllBooks();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                bd.fetchAllBooks(),
                new String[] {"title","authors"},
                new int [] {android.R.id.text1,android.R.id.text2},
                0
                );
        listview.setAdapter(adapter);



        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToFirst();
                Intent intent = new Intent (BibliActivity.this,bookActivity.class);
                Bundle bundle = new Bundle();
                cursor.move(position);
                Book book = BookDbHelper.cursorToBook(cursor);
                bundle.putString("title", book.getTitle());
                bundle.putString("authors", book.getAuthors());
                bundle.putString("year", book.getYear());
                bundle.putString("genre", book.getGenres());
                bundle.putString("publisher", book.getPublisher());
                bundle.putLong("id", book.getId());
                intent.putExtra("data",bundle);

                startActivity(intent);
            }
        });


        listview.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
            {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.menusupp, menu);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BibliActivity.this, bookActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("title"," ");
                bundle.putString("authors"," " );
                bundle.putString("year", " ");
                bundle.putString("genre", " ");
                bundle.putString("publisher", " ");
                bundle.putLong("id", 0);
                intent.putExtra("data",bundle);
                startActivity(intent);
            }

        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final BookDbHelper bd = new BookDbHelper(getApplicationContext());
        final Cursor cursor = bd.fetchAllBooks();
        cursor.move(info.position);
        bd.deleteBook(cursor);
        recreate();

        //return BibliActivity.super.onContextItemSelected(item);
        return true;
    }
}
